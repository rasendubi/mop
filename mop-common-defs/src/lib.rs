#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

mod solver;
mod trait_cfg;
pub mod utils;

pub use crate::{solver::Solver, trait_cfg::TraitCfg};
