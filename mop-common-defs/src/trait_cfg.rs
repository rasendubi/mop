#[cfg(not(feature = "parallel"))]
pub trait TraitCfg {}
#[cfg(not(feature = "parallel"))]
impl<T> TraitCfg for T {}

#[cfg(feature = "parallel")]
pub trait TraitCfg: Send + Sync {}
#[cfg(feature = "parallel")]
impl<T> TraitCfg for T where T: Send + Sync {}
