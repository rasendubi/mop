pub trait Solver {
  type Rslt;

  /// Do solving work after stoping criteria verification.
  fn after_iter(&mut self);

  /// Do solving work before stoping criteria verification.
  fn before_iter(&mut self);

  fn finished(&mut self) {}

  fn init(&mut self) {}

  fn into_result(self) -> Self::Rslt;

  fn result(&self) -> &Self::Rslt;

  fn result_mut(&mut self) -> &mut Self::Rslt;
}
