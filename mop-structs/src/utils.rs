#[cfg(feature = "rayon")]
#[derive(Debug)]
pub struct ParallelIteratorWrapper<I>(pub(crate) I);

#[cfg(feature = "rayon")]
#[derive(Debug)]
pub struct ParallelProducerWrapper<I>(pub(crate) I);

pub(crate) fn are_in_upper_bound<T>(slice: &[T], upper_bound: &T) -> bool
where
  T: PartialOrd,
{
  slice.iter().all(|x| x < upper_bound)
}

pub(crate) fn does_not_have_duplicates<T>(slice: &[T]) -> bool
where
  T: PartialEq,
{
  for (a_idx, a) in slice.iter().enumerate() {
    for b in slice.iter().skip(a_idx + 1) {
      if a == b {
        return false;
      }
    }
  }
  true
}

#[inline]
pub fn matrix_x_y(cols: usize, flattened_idx: usize) -> [usize; 2] {
  [flattened_idx / cols, flattened_idx % cols]
}
