#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

mod array;
mod dense_sto;
mod dim;
pub mod doc_tests;
#[macro_use]
pub mod macros;
pub mod matrix;
pub mod prelude;
pub mod utils;
pub mod vec;
