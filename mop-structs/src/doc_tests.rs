//! Instances for documentation tests.

use crate::matrix::csr_matrix::CsrMatrixArray;
use crate::matrix::csr_matrix::CsrMatrixVecArray;
use crate::matrix::dr_matrix::DrMatrixArray;
use crate::matrix::dr_matrix::DrMatrixVecArray;
use crate::vec::css::CssArray;
use crate::vec::VecArray;

/// ```rust
/// //  _______________________________________
/// // |   | 1 |   | 2 |   |   |   |   |   | 3 |
/// // |   |   |   |   |   |   |   |   |   |   |
/// //  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
/// ```
pub fn css_array() -> CssArray<[i32; 3], [usize; 3]> {
  CssArray::new(10, [1, 2, 3], [1, 3, 9])
}

/// Empty matrix with 5 columns. Supports maximum of 5 values and 4 rows.
pub fn csr_matrix_empty_vec_array() -> CsrMatrixVecArray<[i32; 5], [usize; 5]> {
  CsrMatrixVecArray::with_vec_array(5)
}

/// ```rust
/// //  ___________________
/// // | 1 |   | 2 |   |   |
/// // |___|___|___|___|___|
/// // |   | 3 |   | 4 |   |
/// // |___|___|___|___|___|
/// // |   |   |   |   |   |
/// // |___|___|___|___|___|
/// // |   |   |   |   | 5 |
/// // |   |   |   |   |   |
/// //  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
/// ```
pub fn csr_matrix_array() -> CsrMatrixArray<[i32; 5], [usize; 5]> {
  CsrMatrixArray::new([4, 5], [1, 2, 3, 4, 5], [0, 2, 1, 3, 4], [0, 2, 4, 4, 5])
}

/// Empty matrix with 5 columns. Supports maximum of 20 values and 4 rows.
pub fn dr_matrix_empty_vec_array() -> DrMatrixVecArray<[i32; 20]> {
  DrMatrixVecArray::with_vec_array(5)
}

/// ```rust
/// //  ___________________
/// // | 1 | 2 | 3 | 4 | 5 |
/// // |___|___|___|___|___|
/// // | 6 | 7 | 8 | 9 | 10|
/// // |___|___|___|___|___|
/// // | 11| 12| 13| 14| 15|
/// // |___|___|___|___|___|
/// // | 16| 17| 18| 19| 20|
/// // |   |   |   |   |   |
/// //  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
/// ```
pub fn dr_matrix_array() -> DrMatrixArray<[i32; 20]> {
  DrMatrixArray::new(
    [4, 5],
    [
      1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
    ],
  )
}

/// ```rust
/// //  _______________________________________
/// // | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10|
/// // |   |   |   |   |   |   |   |   |   |   |
/// //  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾
/// ```
pub fn vec_array() -> VecArray<[i32; 16]> {
  use crate::prelude::DynDenseStoMut;
  let mut va = VecArray::<[i32; 16]>::with_capacity();
  va.extend(&[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
  va
}
