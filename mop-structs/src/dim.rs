#[cfg_attr(
  feature = "serde1",
  derive(mop_common_deps::serde::Deserialize, mop_common_deps::serde::Serialize)
)]
#[derive(Clone, Copy, Debug, Default, Hash, PartialEq, PartialOrd)]
pub struct Dim<T> {
  d: T,
}

impl Dim<usize> {
  #[inline]
  pub fn len(self) -> usize {
    self.d
  }

  pub fn len_mut(&mut self) -> &mut usize {
    &mut self.d
  }
}

impl From<usize> for Dim<usize> {
  fn from(from: usize) -> Self {
    Dim { d: from }
  }
}

impl From<Dim<[usize; 2]>> for Dim<usize> {
  fn from(from: Dim<[usize; 2]>) -> Self {
    Dim { d: from.rows() }
  }
}

impl Dim<[usize; 2]> {
  #[inline]
  pub fn cols(&self) -> usize {
    self.d[1]
  }

  #[allow(dead_code)]
  pub fn cols_mut(&mut self) -> &mut usize {
    &mut self.d[1]
  }

  #[inline]
  pub fn rows(&self) -> usize {
    self.d[0]
  }

  pub fn rows_mut(&mut self) -> &mut usize {
    &mut self.d[0]
  }
}

impl From<[usize; 2]> for Dim<[usize; 2]> {
  fn from(from: [usize; 2]) -> Self {
    Dim { d: from }
  }
}
