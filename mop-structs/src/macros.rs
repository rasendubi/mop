#[macro_export]
macro_rules! vec_array {
    (
        [$array_type:ident; $array_len:expr];
        $($x:expr),*
    ) => {
        {
            use mop_structs::{
                vec::VecArray,
                prelude::*
            };
            let mut vec_array: VecArray<[$array_type; $array_len]>;
            vec_array = VecArray::with_capacity();
            $( vec_array.push($x); )*
            vec_array
        }
    }
}
