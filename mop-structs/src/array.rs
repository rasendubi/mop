/// All supported (up to 32) Arrays implement this trait.
pub trait Array {
  const SIZE: usize;
  /// Type
  type Item;

  /// Converts into a mutable slice.
  fn as_mut_slice(&mut self) -> &mut [Self::Item];

  /// Converts into a immutable slice.
  fn as_slice(&self) -> &[Self::Item];
}

macro_rules! array_impls {
    ($($N:expr),+) => {
        $(
            impl<T> Array for [T; $N] {
                const SIZE: usize = $N;
                type Item = T;

                fn as_mut_slice(&mut self) -> &mut [T] {
                    &mut self[..]
                }

                fn as_slice(&self) -> &[T] {
                    &self[..]
                }
            }
        )+
    }
}

array_impls!(
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
  27, 28, 29, 30, 31, 32, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
  0x10000
);
