/// Static Indexed Storage - Reference
pub trait StDenseStoRef {
  type Item;
  fn as_slice(&self) -> &[Self::Item];
}

#[cfg(feature = "std")]
impl<T> StDenseStoRef for Vec<T> {
  type Item = T;
  fn as_slice(&self) -> &[Self::Item] {
    &self[..]
  }
}

impl<'a, T> StDenseStoRef for &'a [T] {
  type Item = T;
  fn as_slice(&self) -> &[Self::Item] {
    &self
  }
}

impl<'a, T> StDenseStoRef for &'a mut [T] {
  type Item = T;
  fn as_slice(&self) -> &[Self::Item] {
    &self
  }
}

macro_rules! array_impls {
    ($($N:expr),+) => {
        $(
            impl<T> StDenseStoRef for [T; $N] {
                type Item = T;
                fn as_slice(&self) -> &[Self::Item] {
                    &self[..]
                }
            }
        )+
    }
}

array_impls!(
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
  27, 28, 29, 30, 31, 32, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
  0x10000
);
