use crate::prelude::DynDenseStoMut;

/// Dynamic Indexed Storage - Reference
pub trait DynDenseSto: DynDenseStoMut {
  fn new() -> Self;
}

#[cfg(feature = "std")]
impl<T> DynDenseSto for Vec<T> {
  fn new() -> Self {
    Vec::new()
  }
}
