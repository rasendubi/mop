use crate::prelude::StDenseStoRef;

/// Static Indexed Storage - Mutable
pub trait StDenseStoMut: StDenseStoRef {
  fn as_mut_slice(&mut self) -> &mut [Self::Item];
}

#[cfg(feature = "std")]
impl<T> StDenseStoMut for Vec<T> {
  fn as_mut_slice(&mut self) -> &mut [Self::Item] {
    &mut self[..]
  }
}

impl<'a, T> StDenseStoMut for &'a mut [T] {
  fn as_mut_slice(&mut self) -> &mut [T] {
    self
  }
}

macro_rules! array_impls {
    ($($N:expr),+) => {
        $(
            impl<T> StDenseStoMut for [T; $N] {
                fn as_mut_slice(&mut self) -> &mut [Self::Item] {
                    &mut self[..]
                }
            }
        )+
    }
}

array_impls!(
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
  27, 28, 29, 30, 31, 32, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
  0x10000
);
