use crate::prelude::StDenseStoRef;

/// Dynamic Indexed Storage - Reference
pub trait DynDenseStoRef: StDenseStoRef {
  fn capacity(&self) -> usize;
}

#[cfg(feature = "std")]
impl<T> DynDenseStoRef for Vec<T> {
  fn capacity(&self) -> usize {
    self.capacity()
  }
}
