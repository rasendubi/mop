use crate::prelude::{DynDenseStoRef, StDenseStoMut};

/// Dynamic Indexed Storage - Mutable
pub trait DynDenseStoMut: DynDenseStoRef + StDenseStoMut {
  fn clear(&mut self);

  fn extend(&mut self, other: &[Self::Item])
  where
    Self::Item: Copy;

  fn extend_from_clone(&mut self, other: &[Self::Item])
  where
    Self::Item: Clone;

  fn push(&mut self, item: Self::Item);

  fn swap(&mut self, a: usize, b: usize);

  fn truncate(&mut self, until_idx: usize);
}

#[cfg(feature = "std")]
impl<T> DynDenseStoMut for Vec<T> {
  fn clear(&mut self) {
    self.clear();
  }

  fn extend(&mut self, other: &[Self::Item])
  where
    Self::Item: Copy,
  {
    core::iter::Extend::extend(self, other);
  }

  fn extend_from_clone(&mut self, other: &[Self::Item])
  where
    Self::Item: Clone,
  {
    self.extend_from_slice(other);
  }

  fn push(&mut self, item: Self::Item) {
    self.push(item);
  }

  fn swap(&mut self, a: usize, b: usize) {
    (&mut **self).swap(a, b);
  }

  fn truncate(&mut self, until_idx: usize) {
    self.truncate(until_idx);
  }
}
