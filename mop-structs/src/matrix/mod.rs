pub mod csr_matrix;
pub mod dr_matrix;

pub trait Matrix {
  fn cols(&self) -> usize;

  fn rows(&self) -> usize;
}
