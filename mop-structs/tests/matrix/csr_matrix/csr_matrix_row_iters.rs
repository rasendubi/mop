use mop_common_deps::quickcheck::quickcheck;
use mop_structs::{matrix::csr_matrix::CsrMatrixVec, prelude::*};

quickcheck! {

fn csr_matrix_row_iter_next(
    cm: CsrMatrixVec<i32>
) -> bool {
    for (row_idx, row) in cm.row_iter().enumerate() {
        if row != cm.row(row_idx) {
            return false;
        }
    }
    true
}

fn csr_matrix_row_iter_next_back(
    cm: CsrMatrixVec<i32>
) -> bool {
    let mut i = cm.row_iter();
    let mut idx = cm.rows();
    while let Some(row) = i.next_back() {
        idx -= 1;
        if row != cm.row(idx) {
            return false;
        }
    }
    true
}

fn csr_matrix_row_iter_split_at(
    cm: CsrMatrixVec<i32>
) -> bool {
    let split_in = if cm.rows() > 3 { 3 } else { cm.rows() };
    let (first_half, second_half) = cm.row_iter().split_at(split_in);
    for (idx, row) in first_half.enumerate() {
        if row != cm.row(idx) {
            return false;
        }
    }
    for (idx, row) in second_half.enumerate() {
        if row != cm.row(idx + split_in) {
            return false;
        }
    }
    true
}

#[cfg(feature = "rayon")]
fn csr_matrix_row_par_iter(
    cm: CsrMatrixVec<i32>
) -> bool {
    cm.row_par_iter().enumerate().all(|(row_idx, row)| row == cm.row(row_idx))
}

}
