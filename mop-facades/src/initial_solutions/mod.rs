mod random_initial_solutions;
mod user_initial_solutions;

pub use self::{
  random_initial_solutions::RandomInitialSolutions, user_initial_solutions::UserInitialSolutions,
};

pub trait InitialSolutions<T> {
  fn initial_solutions(&mut self, t: &mut T);
}
