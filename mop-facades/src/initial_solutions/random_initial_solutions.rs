use crate::{
  initial_solutions::InitialSolutions,
  opt::OptProblem,
  prelude::{Solution, SolutionDomain},
};
use core::default::Default;
use mop_common_deps::rand::{distributions::uniform::SampleUniform, rngs::OsRng};

#[derive(Clone, Debug, Default)]
pub struct RandomInitialSolutions {}

impl RandomInitialSolutions {
  pub fn new() -> Self {
    RandomInitialSolutions {}
  }
}

impl<C, O, OR, S, SD> InitialSolutions<OptProblem<C, O, OR, S, SD>> for RandomInitialSolutions
where
  OR: Default + SampleUniform,
  S: Solution,
  SD: SolutionDomain<S>,
{
  fn initial_solutions(&mut self, op: &mut OptProblem<C, O, OR, S, SD>) {
    let (defs, results) = op.parts_mut();
    let mut rng = OsRng::new().unwrap();
    for _ in 0..defs.results_num() {
      let mut c = results.constructor();
      c = (0..defs.hard_cstrs().len()).fold(c, |c, _| c.push_hard_cstr(usize::default()));
      c = (0..defs.objs().len()).fold(c, |c, _| c.push_obj(OR::default()));
      let domain = defs.solution_domain();
      c.commit(
        OR::default(),
        domain.new_rnd_solution(defs.vars_num().clone(), &mut rng),
      );
    }
  }
}
