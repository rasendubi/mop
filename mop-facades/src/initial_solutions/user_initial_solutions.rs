use crate::{initial_solutions::InitialSolutions, opt::OptProblem, prelude::Solution};
use core::{default::Default, marker::PhantomData};

#[derive(Clone, Debug, Default)]
pub struct UserInitialSolutions<F, S>
where
  F: FnMut(usize) -> S,
{
  cb: F,
  phantom: PhantomData<S>,
}

impl<F, S> UserInitialSolutions<F, S>
where
  F: FnMut(usize) -> S,
{
  pub fn new(cb: F) -> Self {
    UserInitialSolutions {
      cb,
      phantom: PhantomData,
    }
  }
}

impl<C, F, O, OR, S, SD> InitialSolutions<OptProblem<C, O, OR, S, SD>>
  for UserInitialSolutions<F, S>
where
  F: FnMut(usize) -> S,
  OR: Default,
  S: Solution,
{
  fn initial_solutions(&mut self, op: &mut OptProblem<C, O, OR, S, SD>) {
    let (defs, results) = op.parts_mut();
    for idx in 0..defs.results_num() {
      let mut c = results.constructor();
      c = (0..defs.hard_cstrs().len()).fold(c, |c, _| c.push_hard_cstr(usize::default()));
      c = (0..defs.objs().len()).fold(c, |c, _| c.push_obj(OR::default()));
      c.commit(OR::default(), (self.cb)(idx));
    }
  }
}
