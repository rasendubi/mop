use crate::{opt::OptProblem, prelude::Solver};

pub trait OptSolver<C, O, OR, S, SD>: Solver<Rslt = OptProblem<C, O, OR, S, SD>> {
  fn set_opt_problem(&mut self, op: OptProblem<C, O, OR, S, SD>);
}
