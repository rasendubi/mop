use crate::{
  initial_solutions::InitialSolutions,
  opt::{OptFacade, OptHooks, OptHooksFnBuilder, OptProblem},
};
use core::{
  default::Default,
  fmt::Debug,
  ops::{Div, Sub},
};
use mop_blocks::Pct;
use mop_common_deps::num_traits::{NumCast, One, Zero};

#[derive(Debug)]
pub struct OptFacadeBuilder<C, I, O, OH, OR, S, SD> {
  pub(crate) initial_solutions: Option<I>,
  pub(crate) max_iterations: Option<usize>,
  pub(crate) objs_goals: Vec<OR>,
  pub(crate) opt_hooks: Option<OH>,
  pub(crate) opt_problem: Option<OptProblem<C, O, OR, S, SD>>,
  pub(crate) stagnation_percentage: Option<Pct>,
  pub(crate) stagnation_threshold: Option<usize>,
}

impl<C, I, O, OH, OR, S, SD> OptFacadeBuilder<C, I, O, OH, OR, S, SD>
where
  I: InitialSolutions<OptProblem<C, O, OR, S, SD>>,
  OH: OptHooks<S>,
  OR: Copy
    + Debug
    + Div<OR, Output = OR>
    + Default
    + One
    + Zero
    + PartialOrd
    + Send
    + Sub<OR, Output = OR>
    + Sync
    + NumCast,
  S: Copy,
{
  pub fn new() -> Self {
    OptFacadeBuilder {
      initial_solutions: None,
      max_iterations: None,
      objs_goals: vec![],
      opt_hooks: None,
      opt_problem: None,
      stagnation_percentage: None,
      stagnation_threshold: None,
    }
  }

  pub fn build(mut self) -> OptFacade<C, O, OH, OR, S, SD> {
    self
      .initial_solutions
      .unwrap()
      .initial_solutions(self.opt_problem.as_mut().unwrap());
    OptFacade::new(
      self.max_iterations.unwrap(),
      self.objs_goals,
      self.opt_hooks.unwrap(),
      self.opt_problem.unwrap(),
      self.stagnation_percentage.unwrap(),
      self.stagnation_threshold.unwrap(),
    )
  }

  pub fn initial_solutions(mut self, initial_solutions: I) -> Self {
    self.initial_solutions = Some(initial_solutions);
    self
  }

  pub fn max_iterations(mut self, max_iterations: usize) -> Self {
    self.max_iterations = Some(max_iterations);
    self
  }

  pub fn objs_goals(mut self, objs_goals: Vec<OR>) -> Self {
    self.objs_goals = objs_goals;
    self
  }

  pub fn opt_hooks(mut self, opt_hooks: OH) -> Self {
    self.opt_hooks = Some(opt_hooks);
    self
  }

  pub fn opt_problem(mut self, opt_problem: OptProblem<C, O, OR, S, SD>) -> Self {
    self.opt_problem = Some(opt_problem);
    self
  }

  pub fn stagnation_percentage(mut self, stagnation_percentage: Pct) -> Self {
    self.stagnation_percentage = Some(stagnation_percentage);
    self
  }

  pub fn stagnation_threshold(mut self, stagnation_threshold: usize) -> Self {
    self.stagnation_threshold = Some(stagnation_threshold);
    self
  }
}

impl<C, I, O, OH, OR, S, SD> Default for OptFacadeBuilder<C, I, O, OH, OR, S, SD> {
  fn default() -> Self {
    OptFacadeBuilder {
      initial_solutions: None,
      max_iterations: None,
      objs_goals: vec![],
      opt_hooks: None,
      opt_problem: None,
      stagnation_percentage: None,
      stagnation_threshold: None,
    }
  }
}

impl<C, I, O, OR, S, SD> OptFacadeBuilder<C, I, O, (fn(&mut S), fn(&mut S), fn(), fn()), OR, S, SD>
where
  I: InitialSolutions<OptProblem<C, O, OR, S, SD>>,
  OR: Copy
    + Debug
    + Div<OR, Output = OR>
    + Default
    + One
    + Zero
    + PartialOrd
    + Send
    + Sub<OR, Output = OR>
    + Sync
    + NumCast,
  S: Copy,
{
  pub fn with_default_hooks() -> Self {
    OptFacadeBuilder {
      initial_solutions: None,
      max_iterations: None,
      objs_goals: vec![],
      opt_hooks: Some(OptHooksFnBuilder::default().build()),
      opt_problem: None,
      stagnation_percentage: None,
      stagnation_threshold: None,
    }
  }
}
