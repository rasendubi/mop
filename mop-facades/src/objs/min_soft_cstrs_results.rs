use core::fmt::Debug;
use mop_blocks::{
  prelude::{Cstr, Obj},
  ObjDirection,
};
use mop_common_defs::TraitCfg;
use mop_common_deps::num_traits::NumCast;

#[allow(missing_debug_implementations)]
pub struct MinSoftCstrsResults<'a, S> {
  obj_direction: ObjDirection,
  soft_cstrs: Vec<&'a dyn Cstr<S>>,
}

impl<'a, S> MinSoftCstrsResults<'a, S> {
  pub fn new(soft_cstrs: Vec<&'a dyn Cstr<S>>) -> Self {
    let obj_direction = ObjDirection::Min;
    MinSoftCstrsResults {
      obj_direction,
      soft_cstrs,
    }
  }
}

impl<'a, OR, S> Obj<OR, S> for MinSoftCstrsResults<'a, S>
where
  OR: Debug + NumCast + TraitCfg,
  S: Copy + TraitCfg,
{
  fn obj_direction(&self) -> ObjDirection {
    self.obj_direction
  }

  fn result(&self, s: &S) -> OR {
    let sum: usize = self.soft_cstrs.iter().map(|sc| sc.violations(s)).sum();
    OR::from(sum).unwrap()
  }
}
