#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

#[cfg(feature = "std")]
pub mod initial_solutions;
#[cfg(feature = "std")]
pub mod objs;
#[cfg(feature = "std")]
pub mod opt;
#[cfg(feature = "std")]
pub mod prelude;

pub use mop_blocks::{ObjDirection, Pct};
