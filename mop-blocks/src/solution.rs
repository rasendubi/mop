use core::{mem::swap, ops::RangeInclusive};
use mop_common_defs::TraitCfg;
use mop_structs::{
  matrix::{csr_matrix::CsrMatrixVecArray, dr_matrix::DrMatrixVecArray},
  prelude::{Array, Matrix, StDenseStoMut},
  utils::matrix_x_y,
  vec::{css::CssVecArray, VecArray},
};

/// Solution is a set/tuple of flattened and indexed variables.
pub trait Solution: TraitCfg {
  /// The minimum and maximum number of variables the solution can have.
  const VARS_BOUNDS: RangeInclusive<usize>;

  fn has_var(&self, idx: usize) -> bool;

  fn inter_swap(&mut self, other: &mut Self, idx: usize);

  fn intra_swap(&mut self, a: usize, b: usize);

  fn nnz(&self) -> usize;

  fn var_idx(&self, nnz_idx: usize) -> usize;

  fn var_indcs<F>(&self, cb: F)
  where
    F: FnMut(usize);
}

impl<DA, IA> Solution for CsrMatrixVecArray<DA, IA>
where
  DA: Array + TraitCfg,
  IA: Array<Item = usize> + TraitCfg,
{
  const VARS_BOUNDS: RangeInclusive<usize> = 1..=DA::SIZE;

  fn has_var(&self, idx: usize) -> bool {
    let [x, y] = matrix_x_y(self.cols(), idx);
    self.value(x, y).is_some()
  }

  fn inter_swap(&mut self, other: &mut Self, idx: usize) {
    let [x, y] = matrix_x_y(self.cols(), idx);
    swap(
      self.value_mut(x, y).unwrap(),
      other.value_mut(x, y).unwrap(),
    );
  }

  fn intra_swap(&mut self, a: usize, b: usize) {
    let first = matrix_x_y(self.cols(), a);
    let second = matrix_x_y(self.cols(), b);
    self.swap(first, second);
  }

  fn nnz(&self) -> usize {
    self.nnz()
  }

  fn var_idx(&self, nnz_idx: usize) -> usize {
    let row_idx = match self.ptrs().binary_search(&nnz_idx) {
      Ok(x) => x,
      Err(x) => x - 1,
    };
    row_idx * self.cols() + self.indcs()[nnz_idx]
  }

  fn var_indcs<F>(&self, mut cb: F)
  where
    F: FnMut(usize),
  {
    self.ptrs().windows(2).enumerate().for_each(|(idx, range)| {
      let offset = idx * self.cols();
      let cols_indcs = self.indcs()[range[0]..range[1]].iter();
      cols_indcs.for_each(|x| cb(offset + x));
    });
  }
}

impl<DA, IA> Solution for CssVecArray<DA, IA>
where
  DA: Array + TraitCfg,
  IA: Array<Item = usize> + TraitCfg,
{
  const VARS_BOUNDS: RangeInclusive<usize> = 1..=DA::SIZE;

  fn has_var(&self, idx: usize) -> bool {
    self.get(idx).is_some()
  }

  fn inter_swap(&mut self, other: &mut Self, idx: usize) {
    swap(self.get_mut(idx).unwrap(), other.get_mut(idx).unwrap());
  }

  fn intra_swap(&mut self, a: usize, b: usize) {
    self.swap(a, b);
  }

  fn nnz(&self) -> usize {
    self.nnz()
  }

  fn var_idx(&self, nnz_idx: usize) -> usize {
    self.indcs()[nnz_idx]
  }

  fn var_indcs<F>(&self, mut cb: F)
  where
    F: FnMut(usize),
  {
    self.indcs().iter().for_each(|&x| cb(x));
  }
}

impl<DA> Solution for DrMatrixVecArray<DA>
where
  DA: Array + TraitCfg,
{
  const VARS_BOUNDS: RangeInclusive<usize> = 1..=DA::SIZE;

  fn has_var(&self, idx: usize) -> bool {
    let [x, y] = matrix_x_y(self.cols(), idx);
    x < self.rows() && y < self.cols()
  }

  fn inter_swap(&mut self, other: &mut Self, idx: usize) {
    let [x, y] = matrix_x_y(self.cols(), idx);
    swap(self.value_mut(x, y), other.value_mut(x, y));
  }

  fn intra_swap(&mut self, a: usize, b: usize) {
    let first = matrix_x_y(self.cols(), a);
    let second = matrix_x_y(self.cols(), b);
    self.swap(first, second);
  }

  fn nnz(&self) -> usize {
    self.cols() * self.rows()
  }

  fn var_idx(&self, nnz_idx: usize) -> usize {
    nnz_idx
  }

  fn var_indcs<F>(&self, cb: F)
  where
    F: FnMut(usize),
  {
    (0..self.nnz()).for_each(cb);
  }
}

macro_rules! array_impls {
    ($($N:expr),+) => {
        $(
            impl<T> Solution for [T; $N] where T: TraitCfg {
                const VARS_BOUNDS: RangeInclusive<usize> = $N..=$N;

                fn has_var(&self, idx: usize) -> bool {
                    idx < self.len()
                }

                fn inter_swap(&mut self, other: &mut Self, idx: usize) {
                    assert!(idx < self.len());
                    swap(&mut self[idx], &mut other[idx]);
                }

                fn intra_swap(&mut self, a: usize, b: usize) {
                    self.swap(a, b);
                }

                fn nnz(&self) -> usize {
                    $N
                }

                fn var_idx(&self, nnz_idx: usize) -> usize {
                    nnz_idx
                }

                fn var_indcs<F>(&self, cb: F)
                where
                    F: FnMut(usize),
                {
                    (0..$N).for_each(cb);
                }
            }

            impl<T> Solution for VecArray<[T; $N]> where T: TraitCfg {
                const VARS_BOUNDS: RangeInclusive<usize> = 1..=$N;

                fn has_var(&self, idx: usize) -> bool {
                    idx < self.len()
                }

                fn inter_swap(&mut self, other: &mut Self, idx: usize) {
                    assert!(idx < self.len());
                    swap(&mut self.as_mut_slice()[idx], &mut other.as_mut_slice()[idx]);
                }

                fn intra_swap(&mut self, a: usize, b: usize) {
                    self.as_mut_slice().swap(a, b);
                }

                fn nnz(&self) -> usize {
                    self.len()
                }

                fn var_idx(&self, nnz_idx: usize) -> usize {
                    nnz_idx
                }

                fn var_indcs<F>(&self, cb: F)
                where
                    F: FnMut(usize),
                {
                    (0..$N).for_each(cb);
                }
            }
        )+
    }
}

array_impls!(
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26,
  27, 28, 29, 30, 31, 32, 0x40, 0x80, 0x100, 0x200, 0x400, 0x800, 0x1000, 0x2000, 0x4000, 0x8000,
  0x10000
);
