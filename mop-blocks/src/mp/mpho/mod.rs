//! MP (*M*ulti-objective *P*roblem with *H*ard constraints and *O*bjectives)

mod morho;
mod mpho_definitions;
mod mpho_definitions_builder;
mod mpho_evaluators;
pub mod orho;
mod orho_constructor;

pub use self::{
  morho::Morho, mpho_definitions::MphoDefinitions,
  mpho_definitions_builder::MphoDefinitionsBuilder, mpho_evaluators::MphoEvaluators,
  orho_constructor::OrhoConstructor,
};

#[derive(Clone, Debug)]
pub struct Mpho<C, O, OR, S, SD> {
  pub(crate) definitions: MphoDefinitions<C, O, OR, S, SD>,
  pub(crate) morho: Morho<OR, S>,
}

impl<C, O, OR, S, SD> Mpho<C, O, OR, S, SD> {
  pub fn with_capacity(definitions: MphoDefinitions<C, O, OR, S, SD>) -> Self {
    let morho = Morho::with_capacity(definitions.results_num(), &definitions);
    Mpho { definitions, morho }
  }

  pub fn new(definitions: MphoDefinitions<C, O, OR, S, SD>, morho: Morho<OR, S>) -> Self {
    Mpho { definitions, morho }
  }

  pub fn definitions(&self) -> &MphoDefinitions<C, O, OR, S, SD> {
    &self.definitions
  }

  pub fn into_parts(self) -> (MphoDefinitions<C, O, OR, S, SD>, Morho<OR, S>) {
    (self.definitions, self.morho)
  }

  pub fn parts(&self) -> (&MphoDefinitions<C, O, OR, S, SD>, &Morho<OR, S>) {
    (&self.definitions, &self.morho)
  }

  pub fn parts_mut(&mut self) -> (&mut MphoDefinitions<C, O, OR, S, SD>, &mut Morho<OR, S>) {
    (&mut self.definitions, &mut self.morho)
  }

  pub fn results(&self) -> &Morho<OR, S> {
    &self.morho
  }

  pub fn results_mut(&mut self) -> &mut Morho<OR, S> {
    &mut self.morho
  }
}
