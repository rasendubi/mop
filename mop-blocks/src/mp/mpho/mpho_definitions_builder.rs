use crate::{
  mp::mpho::MphoDefinitions,
  prelude::{BorrowAsRef, Cstr, Obj, Solution, SolutionDomain},
};
use core::{default::Default, marker::PhantomData, ops::RangeInclusive};

#[derive(Clone, Debug)]
pub struct MphoDefinitionsBuilder<C, O, OR, S, SD> {
  pub(crate) hard_cstrs: Vec<C>,
  pub(crate) objs: Vec<O>,
  pub(crate) phantom: PhantomData<(OR, S)>,
  pub(crate) results_num: Option<usize>,
  pub(crate) solution_domain: Option<SD>,
  pub(crate) vars_num: Option<RangeInclusive<usize>>,
}

impl<C, O, OR, S, SD> MphoDefinitionsBuilder<C, O, OR, S, SD> {
  pub fn new() -> Self {
    Self::default()
  }

  pub fn build(self) -> MphoDefinitions<C, O, OR, S, SD>
  where
    S: Solution,
    SD: SolutionDomain<S>,
  {
    let solution_domain = self.solution_domain.unwrap();
    let vars_num = if let Some(x) = self.vars_num {
      x
    } else {
      S::VARS_BOUNDS.clone()
    };
    Self::validate_vars_num(&vars_num);
    MphoDefinitions {
      hard_cstrs: self.hard_cstrs,
      objs: self.objs,
      phantom: self.phantom,
      results_num: self.results_num.unwrap(),
      solution_domain,
      vars_num,
    }
  }

  pub fn hard_cstrs(mut self, hard_cstrs: Vec<C>) -> Self {
    self.hard_cstrs = hard_cstrs;
    self
  }

  pub fn objs(mut self, objs: Vec<O>) -> Self {
    self.objs = objs;
    self
  }

  pub fn results_num(mut self, results_num: usize) -> Self {
    self.results_num = Some(results_num);
    self
  }

  pub fn solution_domain(mut self, solution_domain: SD) -> Self {
    self.solution_domain = Some(solution_domain);
    self
  }

  pub fn vars_num(mut self, vars_num: RangeInclusive<usize>) -> Self {
    self.vars_num = Some(vars_num);
    self
  }

  fn validate_vars_num(vars_num: &RangeInclusive<usize>)
  where
    S: Solution,
  {
    assert!(*vars_num.start() > 0);
    assert!(vars_num.start() <= vars_num.end());
    assert!(vars_num.start() >= S::VARS_BOUNDS.start());
    assert!(vars_num.end() <= S::VARS_BOUNDS.end());
  }
}

impl<'a, C, O, OR, S, SD> MphoDefinitionsBuilder<C, O, OR, S, SD>
where
  C: BorrowAsRef<dyn Cstr<S> + 'a>,
  O: BorrowAsRef<dyn Obj<OR, S> + 'a>,
{
  pub fn push_hard_cstr(mut self, hard_cstr: C) -> Self {
    self.hard_cstrs.push(hard_cstr);
    self
  }

  pub fn push_obj(mut self, obj: O) -> Self {
    self.objs.push(obj);
    self
  }
}

impl<C, O, OR, S, SD> Default for MphoDefinitionsBuilder<C, O, OR, S, SD> {
  fn default() -> Self {
    MphoDefinitionsBuilder {
      hard_cstrs: Vec::new(),
      objs: Vec::new(),
      phantom: PhantomData,
      results_num: None,
      solution_domain: None,
      vars_num: None,
    }
  }
}
