//! Orho (*Optimization* *R*esult with *H*ard constraints, and *O*bjectives)

use mop_structs::prelude::{StDenseStoMut, StDenseStoRef};

#[cfg_attr(
  feature = "serde1",
  derive(mop_common_deps::serde::Deserialize, mop_common_deps::serde::Serialize)
)]
#[derive(Debug)]
pub struct Orho<CRNS, CRTS, OR, ORS, S> {
  pub(crate) hard_cstrs_reasons: CRNS,
  pub(crate) hard_cstrs_results: CRTS,
  pub(crate) objs: ORS,
  pub(crate) objs_avg: OR,
  pub(crate) solution: S,
}

pub type OrhoVec<OR, S> = Orho<Vec<String>, Vec<usize>, OR, Vec<OR>, S>;
pub type OrhoRef<'a, OR, S> = Orho<&'a [String], &'a [usize], &'a OR, &'a [OR], &'a S>;
pub type OrhoMut<'a, OR, S> =
  Orho<&'a mut [String], &'a mut [usize], &'a mut OR, &'a mut [OR], &'a mut S>;

impl<CRNS, CRTS, OR, ORS, S> Orho<CRNS, CRTS, OR, ORS, S>
where
  CRNS: StDenseStoRef<Item = String>,
  CRTS: StDenseStoRef<Item = usize>,
  ORS: StDenseStoRef,
{
  pub fn convert_solution<F, AS>(self, mut cb: F) -> Orho<CRNS, CRTS, OR, ORS, AS>
  where
    F: FnMut(S) -> AS,
  {
    Orho {
      hard_cstrs_reasons: self.hard_cstrs_reasons,
      hard_cstrs_results: self.hard_cstrs_results,
      objs: self.objs,
      objs_avg: self.objs_avg,
      solution: cb(self.solution),
    }
  }

  pub fn hard_cstrs_reasons(&self) -> &[String] {
    self.hard_cstrs_reasons.as_slice()
  }

  pub fn hard_cstrs_results(&self) -> &[usize] {
    self.hard_cstrs_results.as_slice()
  }

  pub fn objs(&self) -> &[ORS::Item] {
    self.objs.as_slice()
  }

  pub fn objs_avg(&self) -> &OR {
    &self.objs_avg
  }

  pub fn solution(&self) -> &S {
    &self.solution
  }
}

impl<CRNS, CRTS, OR, ORS, S> Orho<CRNS, CRTS, OR, ORS, S>
where
  CRNS: StDenseStoMut<Item = String>,
  CRTS: StDenseStoMut<Item = usize>,
  ORS: StDenseStoMut,
{
  pub fn hard_cstrs_mut(&mut self) -> (&mut [String], &mut [usize]) {
    (
      self.hard_cstrs_reasons.as_mut_slice(),
      self.hard_cstrs_results.as_mut_slice(),
    )
  }

  pub fn solution_mut(&mut self) -> &mut S {
    &mut self.solution
  }
}

impl<OR, S> OrhoRef<'_, OR, S> {
  pub fn to_vec(&self) -> OrhoVec<OR, S>
  where
    OR: Clone,
    S: Clone,
  {
    OrhoVec {
      hard_cstrs_reasons: self.hard_cstrs_reasons().to_vec(),
      hard_cstrs_results: self.hard_cstrs_results().to_vec(),
      objs: self.objs().to_vec(),
      objs_avg: self.objs_avg.clone(),
      solution: self.solution.clone(),
    }
  }
}
