use crate::mp::mpho::orho::OrhoRef;
use mop_structs::{matrix::dr_matrix::DrMatrixRowConstructor, prelude::DynDenseStoMut};

#[derive(Debug, PartialEq)]
pub struct OrhoConstructor<'a, CRNS, CRTS, ORS, SS> {
  pub(crate) hard_cstrs_reasons: DrMatrixRowConstructor<'a, CRNS>,
  pub(crate) hard_cstrs_results: DrMatrixRowConstructor<'a, CRTS>,
  pub(crate) objs: DrMatrixRowConstructor<'a, ORS>,
  pub(crate) objs_avg: &'a mut ORS,
  pub(crate) solutions: &'a mut SS,
}

impl<'a, CRNS, CRTS, ORS, SS> OrhoConstructor<'a, CRNS, CRTS, ORS, SS>
where
  CRNS: DynDenseStoMut<Item = String>,
  CRTS: DynDenseStoMut<Item = usize>,
  ORS: DynDenseStoMut,
  SS: DynDenseStoMut,
{
  pub fn commit(self, objs_avg: ORS::Item, solution: SS::Item) {
    self.hard_cstrs_reasons.commit();
    self.hard_cstrs_results.commit();
    self.objs.commit();
    self.objs_avg.push(objs_avg);
    self.solutions.push(solution);
  }

  pub fn copy_result(mut self, from: &OrhoRef<'_, ORS::Item, SS::Item>) -> Self
  where
    ORS::Item: Copy,
    SS::Item: Copy,
  {
    self.hard_cstrs_reasons = self
      .hard_cstrs_reasons
      .clone_values_from_row(from.hard_cstrs_reasons);
    self.hard_cstrs_results = self
      .hard_cstrs_results
      .copy_values_from_row(from.hard_cstrs_results);
    self.objs = self.objs.copy_values_from_row(from.objs);
    self
  }

  pub fn push_hard_cstr(mut self, result: CRTS::Item) -> Self {
    self.hard_cstrs_reasons = self.hard_cstrs_reasons.push_value(String::new());
    self.hard_cstrs_results = self.hard_cstrs_results.push_value(result);
    self
  }

  pub fn push_obj(mut self, value: ORS::Item) -> Self {
    self.objs = self.objs.push_value(value);
    self
  }
}
