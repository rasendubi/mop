use crate::{
  mp::mpho::{orho::OrhoMut, Morho, MphoDefinitions},
  prelude::BorrowAsRef,
  prelude::{Cstr, Obj},
};
use core::{fmt::Debug, marker::PhantomData, ops::Div};
use mop_common_deps::num_traits::{NumCast, Zero};
#[cfg(feature = "rayon")]
use mop_common_deps::rayon::prelude::*;

#[derive(Debug)]
pub struct MphoEvaluators<C, O, OR, S, SD> {
  phantom: PhantomData<(C, O, OR, S, SD)>,
}

impl<'a, C, O, OR, S, SD> MphoEvaluators<C, O, OR, S, SD>
where
  C: BorrowAsRef<dyn Cstr<S> + 'a>,
  O: BorrowAsRef<dyn Obj<OR, S> + 'a>,
  OR: Copy + Debug + Div<OR, Output = OR> + NumCast + Zero,
{
  #[cfg(feature = "rayon")]
  pub fn eval_results(definitions: &MphoDefinitions<C, O, OR, S, SD>, results: &mut Morho<OR, S>)
  where
    C: Send + Sync,
    OR: Send + Sync,
    O: Send + Sync,
    S: Send + Sync,
    SD: Send + Sync,
  {
    results
      .par_iter_mut()
      .for_each(|mut x| Self::do_eval_results(definitions, &mut x));
  }

  #[cfg(not(feature = "rayon"))]
  pub fn eval_results(definitions: &MphoDefinitions<C, O, OR, S, SD>, results: &mut Morho<OR, S>) {
    results
      .iter_mut()
      .for_each(|mut x| Self::do_eval_results(definitions, &mut x));
  }

  #[cfg(feature = "rayon")]
  pub fn eval_reasons(definitions: &MphoDefinitions<C, O, OR, S, SD>, results: &mut Morho<OR, S>)
  where
    C: Send + Sync,
    OR: Send + Sync,
    O: Send + Sync,
    S: Send + Sync,
    SD: Send + Sync,
  {
    results
      .par_iter_mut()
      .for_each(|mut x| Self::do_eval_reasons(definitions, &mut x));
  }

  #[cfg(not(feature = "rayon"))]
  pub fn eval_reasons(definitions: &MphoDefinitions<C, O, OR, S, SD>, results: &mut Morho<OR, S>) {
    results
      .iter_mut()
      .for_each(|mut x| Self::do_eval_reasons(definitions, &mut x));
  }

  fn do_eval_reasons(
    definitions: &MphoDefinitions<C, O, OR, S, SD>,
    result: &mut OrhoMut<'_, OR, S>,
  ) {
    Self::eval_cstr_reasons(
      &definitions.hard_cstrs,
      &mut result.hard_cstrs_reasons,
      result.solution,
    );
  }

  fn do_eval_results(
    definitions: &MphoDefinitions<C, O, OR, S, SD>,
    result: &mut OrhoMut<'_, OR, S>,
  ) {
    Self::eval_cstr_results(
      &definitions.hard_cstrs,
      &mut result.hard_cstrs_results,
      result.solution,
    );
    Self::eval_objs_results(&definitions.objs, result);
  }

  fn eval_cstr_reasons(criterias: &[C], reasons: &mut [String], solution: &S) {
    for (sc, scr) in criterias.iter().zip(reasons.iter_mut()) {
      *scr = sc.borrow().reasons(solution);
    }
  }

  fn eval_cstr_results(criterias: &[C], results: &mut [usize], solution: &S) {
    for (sc, scr) in criterias.iter().zip(results.iter_mut()) {
      let borrow = sc.borrow();
      *scr = borrow.violations(solution);
    }
  }

  fn eval_objs_results(objs: &[O], result: &mut OrhoMut<'_, OR, S>) {
    let mut objs_avg = OR::zero();
    for (obj, obj_rslts) in objs.iter().zip(result.objs.iter_mut()) {
      let rslt = obj.borrow().result(&result.solution);
      *obj_rslts = rslt;
      objs_avg = objs_avg + rslt;
    }
    let objs_len = objs.len();
    let cast = NumCast::from(objs_len).unwrap();
    *result.objs_avg = objs_avg / cast;
  }
}
