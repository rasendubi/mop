//! Gmor (*M*any *O*ptimization* *R*esults with *H*ard constraints and *O*bjectives)
use crate::mp::mpho::{
  orho::{OrhoMut, OrhoRef},
  MphoDefinitions, OrhoConstructor,
};
#[cfg(feature = "rayon")]
use mop_common_deps::rayon::prelude::*;
use mop_structs::matrix::dr_matrix::DrMatrixVec;

#[derive(Clone, Debug)]
pub struct Morho<OR, S> {
  pub(crate) best_idx: Option<usize>,
  pub(crate) hard_cstrs_reasons: DrMatrixVec<String>,
  pub(crate) hard_cstrs_results: DrMatrixVec<usize>,
  pub(crate) objs: DrMatrixVec<OR>,
  pub(crate) objs_avg: Vec<OR>,
  pub(crate) solutions: Vec<S>,
}

impl<OR, S> Morho<OR, S> {
  pub fn with_capacity<C, O, SD>(capacity: usize, defs: &MphoDefinitions<C, O, OR, S, SD>) -> Self {
    let hard_cstrs_reasons = DrMatrixVec::with_vec_capacity([capacity, defs.hard_cstrs().len()]);
    let hard_cstrs_results = DrMatrixVec::with_vec_capacity([capacity, defs.hard_cstrs().len()]);
    let objs = DrMatrixVec::with_vec_capacity([capacity, defs.objs().len()]);
    let objs_avg = Vec::with_capacity(capacity);
    let solutions = Vec::with_capacity(capacity);
    Morho {
      best_idx: None,
      hard_cstrs_reasons,
      hard_cstrs_results,
      objs,
      objs_avg,
      solutions,
    }
  }
}

impl<OR, S> Morho<OR, S> {
  pub fn best(&self) -> Option<OrhoRef<'_, OR, S>> {
    self.best_idx.map(|x| OrhoRef {
      hard_cstrs_reasons: self.hard_cstrs_reasons.row(x),
      hard_cstrs_results: self.hard_cstrs_results.row(x),
      objs: self.objs.row(x),
      objs_avg: &self.objs_avg[x],
      solution: &self.solutions[x],
    })
  }

  pub fn best_objs(&self) -> Option<&[OR]> {
    self.best_idx.map(|x| self.objs.row(x))
  }

  pub fn clear(&mut self) {
    self.hard_cstrs_reasons.clear();
    self.hard_cstrs_results.clear();
    self.objs.clear();
    self.objs_avg.clear();
    self.solutions.clear();
  }

  pub fn constructor(&mut self) -> OrhoConstructor<'_, Vec<String>, Vec<usize>, Vec<OR>, Vec<S>> {
    OrhoConstructor {
      hard_cstrs_reasons: self.hard_cstrs_reasons.row_constructor(),
      hard_cstrs_results: self.hard_cstrs_results.row_constructor(),
      objs: self.objs.row_constructor(),
      objs_avg: &mut self.objs_avg,
      solutions: &mut self.solutions,
    }
  }

  pub fn extend(&mut self, other: &Self)
  where
    OR: Copy,
    S: Copy,
  {
    self
      .hard_cstrs_reasons
      .extend_from_clone(&other.hard_cstrs_reasons);
    self.hard_cstrs_results.extend(&other.hard_cstrs_results);
    self.objs.extend(&other.objs);
    self.objs_avg.extend(&other.objs_avg);
    self.solutions.extend(&other.solutions);
  }

  pub fn get(&self, idx: usize) -> OrhoRef<'_, OR, S> {
    OrhoRef {
      hard_cstrs_reasons: self.hard_cstrs_reasons.row(idx),
      hard_cstrs_results: self.hard_cstrs_results.row(idx),
      objs: self.objs.row(idx),
      objs_avg: &self.objs_avg[idx],
      solution: &self.solutions[idx],
    }
  }

  pub fn get_mut(&mut self, idx: usize) -> OrhoMut<'_, OR, S> {
    OrhoMut {
      hard_cstrs_reasons: self.hard_cstrs_reasons.row_mut(idx),
      hard_cstrs_results: self.hard_cstrs_results.row_mut(idx),
      objs: self.objs.row_mut(idx),
      objs_avg: &mut self.objs_avg.as_mut_slice()[idx],
      solution: &mut self.solutions[idx],
    }
  }

  pub fn iter(&self) -> impl Iterator<Item = OrhoRef<'_, OR, S>> {
    let hard_cstrs = self
      .hard_cstrs_reasons
      .row_iter()
      .zip(self.hard_cstrs_results.row_iter());
    hard_cstrs
      .zip(
        self
          .objs
          .row_iter()
          .zip(self.objs_avg.iter().zip(self.solutions.iter())),
      )
      .map(|(hard_cstrs, (objs, (objs_avg, solution)))| OrhoRef {
        hard_cstrs_reasons: hard_cstrs.0,
        hard_cstrs_results: hard_cstrs.1,
        objs,
        objs_avg,
        solution,
      })
  }

  pub fn iter_mut(&mut self) -> impl Iterator<Item = OrhoMut<'_, OR, S>> {
    let hard_cstrs = self
      .hard_cstrs_reasons
      .row_iter_mut()
      .zip(self.hard_cstrs_results.row_iter_mut());
    hard_cstrs
      .zip(
        self
          .objs
          .row_iter_mut()
          .zip(self.objs_avg.iter_mut().zip(self.solutions.iter_mut())),
      )
      .map(|(hard_cstrs, (objs, (objs_avg, solution)))| OrhoMut {
        hard_cstrs_reasons: hard_cstrs.0,
        hard_cstrs_results: hard_cstrs.1,
        objs,
        objs_avg,
        solution,
      })
  }

  #[inline]
  pub fn len(&self) -> usize {
    self.solutions.len()
  }

  #[cfg(feature = "rayon")]
  pub fn par_iter(&self) -> impl IndexedParallelIterator<Item = OrhoRef<'_, OR, S>>
  where
    OR: Send + Sync,
    S: Send + Sync,
  {
    let hard_cstrs = self
      .hard_cstrs_reasons
      .row_par_iter()
      .zip(self.hard_cstrs_results.row_par_iter());
    hard_cstrs
      .zip_eq(
        self
          .objs
          .row_par_iter()
          .zip_eq(self.objs_avg.par_iter().zip_eq(self.solutions.par_iter())),
      )
      .map(|(hard_cstrs, (objs, (objs_avg, solution)))| OrhoRef {
        hard_cstrs_reasons: hard_cstrs.0,
        hard_cstrs_results: hard_cstrs.1,
        objs,
        objs_avg,
        solution,
      })
  }

  #[cfg(feature = "rayon")]
  pub fn par_iter_mut(&mut self) -> impl IndexedParallelIterator<Item = OrhoMut<'_, OR, S>>
  where
    OR: Send + Sync,
    S: Send + Sync,
  {
    let hard_cstrs = self
      .hard_cstrs_reasons
      .row_par_iter_mut()
      .zip(self.hard_cstrs_results.row_par_iter_mut());
    hard_cstrs
      .zip_eq(
        self.objs.row_par_iter_mut().zip_eq(
          self
            .objs_avg
            .as_mut_slice()
            .par_iter_mut()
            .zip_eq(self.solutions.par_iter_mut()),
        ),
      )
      .map(|(hard_cstrs, (objs, (objs_avg, solution)))| OrhoMut {
        hard_cstrs_reasons: hard_cstrs.0,
        hard_cstrs_results: hard_cstrs.1,
        objs,
        objs_avg,
        solution,
      })
  }

  pub fn set_best(&mut self, index: usize) {
    self.best_idx = Some(index);
  }

  pub fn get_two_mut(&mut self, smaller_idx: usize, bigger_idx: usize) -> [OrhoMut<'_, OR, S>; 2] {
    assert!(smaller_idx < bigger_idx && smaller_idx < self.len() && bigger_idx < self.len());
    let [first_o, second_o] = self.objs.two_rows_mut(smaller_idx, bigger_idx);
    let [first_oa, second_oa] = {
      let (first, second) = self.objs_avg.split_at_mut(bigger_idx);
      [&mut first[smaller_idx], &mut second[0]]
    };
    let [first_hcrea, second_hcrea] = self
      .hard_cstrs_reasons
      .two_rows_mut(smaller_idx, bigger_idx);
    let [first_hcres, second_hcres] = self
      .hard_cstrs_results
      .two_rows_mut(smaller_idx, bigger_idx);
    let [first_s, second_s] = {
      let (first, second) = self.solutions.split_at_mut(bigger_idx);
      [&mut first[smaller_idx], &mut second[0]]
    };
    [
      OrhoMut {
        hard_cstrs_reasons: first_hcrea,
        hard_cstrs_results: first_hcres,
        objs: first_o,
        objs_avg: first_oa,
        solution: first_s,
      },
      OrhoMut {
        hard_cstrs_reasons: second_hcrea,
        hard_cstrs_results: second_hcres,
        objs: second_o,
        objs_avg: second_oa,
        solution: second_s,
      },
    ]
  }

  pub fn truncate(&mut self, until_idx: usize) {
    self.hard_cstrs_reasons.truncate(until_idx);
    self.hard_cstrs_results.truncate(until_idx);
    self.objs.truncate(until_idx);
    self.objs_avg.truncate(until_idx);
    self.solutions.truncate(until_idx);
  }
}
