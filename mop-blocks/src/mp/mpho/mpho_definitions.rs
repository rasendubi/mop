use crate::mp::mpho::MphoDefinitionsBuilder;
use core::{marker::PhantomData, ops::RangeInclusive};

#[derive(Clone, Debug)]
pub struct MphoDefinitions<C, O, OR, S, SD> {
  pub(crate) hard_cstrs: Vec<C>,
  pub(crate) objs: Vec<O>,
  pub(crate) phantom: PhantomData<(OR, S)>,
  pub(crate) results_num: usize,
  pub(crate) solution_domain: SD,
  pub(crate) vars_num: RangeInclusive<usize>,
}

impl<C, O, OR, S, SD> MphoDefinitions<C, O, OR, S, SD> {
  /// Hard constraints
  pub fn hard_cstrs(&self) -> &[C] {
    &self.hard_cstrs
  }

  /// Converts into `MphoDefinitionsBuilder` for possible adjustments.
  pub fn into_builder(self) -> MphoDefinitionsBuilder<C, O, OR, S, SD> {
    MphoDefinitionsBuilder {
      hard_cstrs: self.hard_cstrs,
      objs: self.objs,
      phantom: self.phantom,
      results_num: Some(self.results_num),
      solution_domain: Some(self.solution_domain),
      vars_num: Some(self.vars_num),
    }
  }

  /// Objectives
  pub fn objs(&self) -> &[O] {
    &self.objs
  }

  /// Results number
  pub fn results_num(&self) -> usize {
    self.results_num
  }

  pub fn solution_domain(&self) -> &SD {
    &self.solution_domain
  }

  pub fn solution_domain_mut(&mut self) -> &mut SD {
    &mut self.solution_domain
  }

  pub fn vars_num(&self) -> &RangeInclusive<usize> {
    &self.vars_num
  }
}
