use mop_common_defs::TraitCfg;

pub trait BorrowAsRef<T>: TraitCfg
where
  T: ?Sized,
{
  fn borrow(&self) -> &T;
}
