//! Objective

use crate::{prelude::BorrowAsRef, ObjDirection};
use core::fmt::Debug;
use mop_common_defs::TraitCfg;

/// Constraint
pub trait Obj<OR, S>: TraitCfg {
  fn obj_direction(&self) -> ObjDirection;

  fn result(&self, solution: &S) -> OR;
}

impl<'a, OR, S, T> Obj<OR, S> for &'a T
where
  T: Obj<OR, S> + ?Sized + 'a,
{
  fn obj_direction(&self) -> ObjDirection {
    (&**self).obj_direction()
  }

  fn result(&self, solution: &S) -> OR {
    (&**self).result(solution)
  }
}

impl<OR, S> Obj<OR, S> for (ObjDirection, fn(&S) -> OR)
where
  OR: Debug + TraitCfg,
{
  fn obj_direction(&self) -> ObjDirection {
    self.0
  }

  fn result(&self, solution: &S) -> OR {
    (self.1)(solution)
  }
}

impl<'a, OR, S, T> BorrowAsRef<dyn Obj<OR, S> + 'a> for T
where
  T: Obj<OR, S> + 'a,
{
  fn borrow(&self) -> &(dyn Obj<OR, S> + 'a) {
    &*self
  }
}
