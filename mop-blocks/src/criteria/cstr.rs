//! Constraint

use crate::prelude::BorrowAsRef;
use mop_common_defs::TraitCfg;

/// Constraint
pub trait Cstr<S>: TraitCfg {
  #[cfg(feature = "std")]
  fn reasons(&self, _: &S) -> String {
    String::new()
  }

  #[cfg(not(feature = "std"))]
  fn reasons(&self, _: &S) -> &'static str {
    ""
  }

  fn violations(&self, solution: &S) -> usize;
}

impl<'a, S, T> Cstr<S> for &'a T
where
  T: Cstr<S> + ?Sized + 'a,
{
  #[cfg(feature = "std")]
  fn reasons(&self, solution: &S) -> String {
    (&**self).reasons(solution)
  }

  #[cfg(not(feature = "std"))]
  fn reasons(&self, solution: &S) -> &'static str {
    (&**self).reasons(solution)
  }

  fn violations(&self, solution: &S) -> usize {
    (&**self).violations(solution)
  }
}

impl<S> Cstr<S> for ()
where
  S: TraitCfg,
{
  fn violations(&self, _: &S) -> usize {
    usize::default()
  }
}

impl<S> Cstr<S> for fn(&S) -> usize
where
  S: TraitCfg,
{
  fn violations(&self, solution: &S) -> usize {
    self(solution)
  }
}

impl<S> Cstr<S> for (usize, fn(&S) -> usize) {
  fn violations(&self, solution: &S) -> usize {
    self.1(solution)
  }
}

#[cfg(feature = "std")]
impl<S> Cstr<S> for (fn(&S) -> String, fn(&S) -> usize) {
  fn reasons(&self, solution: &S) -> String {
    self.0(solution)
  }

  fn violations(&self, solution: &S) -> usize {
    self.1(solution)
  }
}

#[cfg(not(feature = "std"))]
impl<S> Cstr<S> for (fn(&S) -> &'static str, fn(&S) -> usize) {
  fn reasons(&self, solution: &S) -> &'static str {
    self.0(solution)
  }

  fn violations(&self, solution: &S) -> usize {
    self.1(solution)
  }
}

#[cfg(feature = "std")]
impl<S> Cstr<S> for (usize, fn(&S) -> String, fn(&S) -> usize) {
  fn reasons(&self, solution: &S) -> String {
    self.1(solution)
  }

  fn violations(&self, solution: &S) -> usize {
    self.2(solution)
  }
}

#[cfg(not(feature = "std"))]
impl<S> Cstr<S> for (usize, fn(&S) -> &'static str, fn(&S) -> usize) {
  fn reasons(&self, solution: &S) -> &'static str {
    self.1(solution)
  }

  fn violations(&self, solution: &S) -> usize {
    self.2(solution)
  }
}

impl<'a, S, T> BorrowAsRef<dyn Cstr<S> + 'a> for T
where
  T: Cstr<S> + 'a,
{
  fn borrow(&self) -> &(dyn Cstr<S> + 'a) {
    &*self
  }
}
