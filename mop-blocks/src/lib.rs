//! Mop (Many Optimizations)

#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

mod borrow_as_ref;
mod criteria;
#[cfg(feature = "std")]
pub mod mp;
mod obj_direction;
mod pct;
pub mod prelude;
mod solution;
mod solution_domain;

pub use crate::{obj_direction::ObjDirection, pct::Pct};
