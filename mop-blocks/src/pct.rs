use core::ops::Deref;
use mop_common_deps::rand::Rng;

/// Percent in decimal format
#[derive(Clone, Copy, Debug)]
pub struct Pct(f64);

impl Pct {
  pub fn from_decimal<T>(pct: T) -> Self
  where
    T: Into<f64>,
  {
    Pct(pct.into())
  }

  pub fn from_percent<T>(pct: T) -> Self
  where
    T: Into<f64>,
  {
    Pct(pct.into() / 100.0)
  }

  pub fn is_in_rnd_pbty<R>(self, rng: &mut R) -> bool
  where
    R: Rng,
  {
    rng.gen::<f64>() < self.0
  }
}

impl Deref for Pct {
  type Target = f64;
  fn deref(&self) -> &f64 {
    &self.0
  }
}
