#!/usr/bin/env bash
set -e

# Based on https://github.com/SergioBenitez/Rocket/blob/master/scripts/test.sh

COMMON_FEATURES=(
    "rayon std"
    serde1
    std
)

COMMON_PACKAGES=(
    mop-adapters
    mop-blocks
    mop-facades
    mop-solvers
)

# Utils

test_package_generic() {
    local package=$1

    /bin/echo -e "\e[0;33m***** Testing ${package} without features *****\e[0m\n"
    cargo test --manifest-path "${package}"/Cargo.toml --no-default-features

    /bin/echo -e "\e[0;33m***** Testing ${package} with all features *****\e[0m\n"
    cargo test --manifest-path "${package}"/Cargo.toml --all-features
}

test_package_with_common_features() {
    local package=$1

    for feature in "${COMMON_FEATURES[@]}"; do
        /bin/echo -e "\e[0;33m***** Testing ${package} with '${feature}' *****\e[0m\n"
        cargo test --manifest-path "${package}"/Cargo.toml --features "${feature}" --no-default-features
    done
}

test_package_with_feature() {
    local package=$1
    local feature=$2

    /bin/echo -e "\e[0;33m***** Testing ${package} with feature '${feature}' *****\e[0m\n"
    cargo test --manifest-path "${package}"/Cargo.toml --features "${feature}" --no-default-features
}

# Common packages with common features

/bin/echo -e "\e[0;34m***** Testing common packages with common features *****\e[0m\n"

for package in "${COMMON_PACKAGES[@]}"; do
    test_package_generic $package
    test_package_with_common_features $package
done

# Selected packages with selected features

/bin/echo -e "\e[0;34m***** Testing selected packages with common and selected features *****\e[0m\n"

test_package_generic "mop-bindings"
test_package_with_feature "mop-bindings" "wasm"

test_package_generic "mop-common-defs"
test_package_with_feature "mop-common-defs" "parallel"