//! MOP (*M*any *OP*timizations)

pub extern crate mop_adapters as adapters;
pub extern crate mop_facades as facades;
pub extern crate mop_solvers as solvers;
pub use mop_common_deps::rand;
