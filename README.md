# MOP (Many OPtimizations)

[MOP](https://gitlab.com/c410-f3r/mop) is a flexible and modular single or multi-objective solver for contiguous and discrete problems. Through its default pipeline you can define your own custom problem and choose any supported solver combination.

## Current state

Testing and documentation are scarce and the project itself is in **experimental** phase. Nevertheless, things seems to work in general.

## Solvers

Official built-in solvers.

- [ ] Spea2 (Mostly done)

## Objectives

Some ideas to guide development.

### Short-term objectives

- [ ] Add more tests
- [ ] Complete API documentation
- [ ] Enchance integrity and robustness

### Mid-term objectives

- [ ] Add more solvers

### Humble long-long-term objectives

- [ ] Support linear programming
- [ ] Support quadratic programming
- [ ] Be at least as fast as commercial solvers