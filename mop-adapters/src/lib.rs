#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

#[cfg(feature = "std")]
mod spea2_opt_solver;

#[cfg(feature = "std")]
pub use crate::spea2_opt_solver::Spea2OptSolver;
