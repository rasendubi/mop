use core::{
  fmt::Debug,
  ops::{Div, Mul, Sub},
};
use mop_common_deps::rand::distributions::uniform::SampleUniform;
use mop_facades::{
  opt::{OptProblem, OptProblemResults},
  prelude::{BorrowAsRef, Cstr, Obj, OptSolver},
  Pct,
};
use mop_solvers::{
  genetic_algorithm::{
    operators::{crossover::Crossover, mating_selection::MatingSelection, mutation::Mutation},
    spea2::Spea2,
    GeneticAlgorithmParams,
  },
  prelude::{NumCast, One, Pow, Zero},
  Solver,
};

#[derive(Debug)]
pub struct Spea2OptSolver<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  MPOR: Debug,
{
  partial: Option<(Pct, GeneticAlgorithmParams<GAPC, GAPM, GAPMS>)>,
  spea2: Option<Spea2<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>>,
}

impl<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
  Spea2OptSolver<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  MPOR: Debug
    + Copy
    + Div<MPOR, Output = MPOR>
    + Mul<MPOR, Output = MPOR>
    + NumCast
    + One
    + PartialOrd
    + Pow<MPOR, Output = MPOR>
    + Send
    + Sub<MPOR, Output = MPOR>
    + Sync
    + Zero,
{
  pub fn new(archive_size: Pct, gap: GeneticAlgorithmParams<GAPC, GAPM, GAPMS>) -> Self {
    Spea2OptSolver {
      partial: Some((archive_size, gap)),
      spea2: None,
    }
  }
}

impl<'a, GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD> Solver
  for Spea2OptSolver<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  GAPC: Crossover<OptProblemResults<MPOR, MPS>>,
  GAPM: Mutation<MPSD, OptProblemResults<MPOR, MPS>>,
  GAPMS: MatingSelection<[MPO], OptProblemResults<MPOR, MPS>>,
  MPC: BorrowAsRef<dyn Cstr<MPS> + 'a>,
  MPO: BorrowAsRef<dyn Obj<MPOR, MPS> + 'a>,
  MPOR: Copy
    + Debug
    + Div<MPOR, Output = MPOR>
    + Mul<MPOR, Output = MPOR>
    + NumCast
    + One
    + PartialOrd
    + Pow<MPOR, Output = MPOR>
    + SampleUniform
    + Send
    + Sub<MPOR, Output = MPOR>
    + Sync
    + Zero,
  MPS: Copy + Send + Sync,
  MPSD: Send + Sync,
{
  type Rslt = OptProblem<MPC, MPO, MPOR, MPS, MPSD>;

  fn after_iter(&mut self) {
    self.spea2.as_mut().unwrap().after_iter();
  }

  fn before_iter(&mut self) {
    self.spea2.as_mut().unwrap().before_iter();
  }

  fn init(&mut self) {
    self.spea2.as_mut().unwrap().init();
  }

  fn into_result(self) -> Self::Rslt {
    self.spea2.unwrap().into_result()
  }

  fn finished(&mut self) {
    self.spea2.as_mut().unwrap().finished()
  }

  fn result(&self) -> &Self::Rslt {
    self.spea2.as_ref().unwrap().result()
  }

  fn result_mut(&mut self) -> &mut Self::Rslt {
    self.spea2.as_mut().unwrap().result_mut()
  }
}

impl<'a, GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD> OptSolver<MPC, MPO, MPOR, MPS, MPSD>
  for Spea2OptSolver<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  GAPC: Crossover<OptProblemResults<MPOR, MPS>>,
  GAPM: Mutation<MPSD, OptProblemResults<MPOR, MPS>>,
  GAPMS: MatingSelection<[MPO], OptProblemResults<MPOR, MPS>>,
  MPC: BorrowAsRef<dyn Cstr<MPS> + 'a>,
  MPO: BorrowAsRef<dyn Obj<MPOR, MPS> + 'a>,
  MPOR: Copy
    + Debug
    + Div<MPOR, Output = MPOR>
    + Mul<MPOR, Output = MPOR>
    + NumCast
    + One
    + PartialOrd
    + Pow<MPOR, Output = MPOR>
    + SampleUniform
    + Send
    + Sub<MPOR, Output = MPOR>
    + Sync
    + Zero,
  MPS: Copy + Send + Sync,
  MPSD: Send + Sync,
{
  fn set_opt_problem(&mut self, op: OptProblem<MPC, MPO, MPOR, MPS, MPSD>) {
    let (archive_size, spea2) = self.partial.take().unwrap();
    self.spea2 = Some(Spea2::new(archive_size, spea2, op))
  }
}
