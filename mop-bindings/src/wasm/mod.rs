use core::ops::{Range, RangeInclusive};
use js_sys::{Array, Function};
use mop::{
  genetic_algorithm::{
    operators::{
      crossover::MultiPoint, mating_selection::Tournament, mutation::RandomDomainAssignments,
    },
    spea2::Spea2Adapter,
    GeneticAlgorithmParamsBuilder,
  },
  initial_solutions::RandomInitialSolutions,
  opt_problem::{
    Cstr, Domain, Obj, ObjDirection, OptProblem, OptProblemDefinitions,
    OptProblemDefinitionsBuilder, OptProblemResultVec, OptProblemResults, Pct, Solution,
  },
  OptFacade, OptFacadeBuilder,
};
use mop_common_deps::{rand::Rng, wasm_bindgen::prelude::*};
use mop_structs::{
  prelude::{DynDenseStoMut, StDenseStoRef},
  vec::VecArray,
};

// DomainJs

#[wasm_bindgen]
#[derive(Debug, Clone)]
pub struct DomainJs(VecArray<[RangeInclusive<f64>; 16]>);

#[wasm_bindgen]
impl DomainJs {
  #[wasm_bindgen(constructor)]
  pub fn new(ranges: Vec<JsValue>) -> Self {
    let mut va = VecArray::with_capacity();
    ranges.iter().for_each(|x| {
      let string = x.as_string().unwrap();
      let mut iter = string.split('=');
      let lower_bound = Self::parse_bound(iter.next());
      let upper_bound = Self::parse_bound(iter.next());
      va.push(lower_bound..=upper_bound);
    });

    DomainJs(va)
  }

  fn parse_bound(bound_option: Option<&str>) -> f64 {
    if let Some(bound_str) = bound_option {
      if let Ok(bound) = bound_str.parse::<f64>() {
        bound
      } else {
        panic!("Couldn't parse domain range into a floating number");
      }
    } else {
      panic!("Empty left or right bound for range domain");
    }
  }
}

impl Domain<SolutionJs> for DomainJs {
  fn len(&self) -> usize {
    self.0.len()
  }

  fn new_rnd_solution<R>(&self, vars_num: Range<usize>, rng: &mut R) -> SolutionJs
  where
    R: Rng,
  {
    SolutionJs(self.0.new_rnd_solution(vars_num, rng))
  }

  fn set_rnd_solution_domain<R>(&self, s: &mut SolutionJs, idx: usize, rng: &mut R)
  where
    R: Rng,
  {
    self.0.set_rnd_solution_domain(&mut s.0, idx, rng);
  }
}

// HardCstr

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct HardCstrJs(Function);

#[wasm_bindgen]
impl HardCstrJs {
  #[wasm_bindgen(constructor)]
  pub fn new(f: Function) -> Self {
    HardCstrJs(f)
  }
}

impl Cstr<SolutionJs> for HardCstrJs {
  fn violations(&self, solution: &SolutionJs) -> usize {
    let array: Array = Array::new();
    solution.0.iter().for_each(|&x| {
      let jv = JsValue::from_f64(x);
      array.push(&jv);
    });
    self
      .0
      .call1(&JsValue::NULL, &JsValue::from(array))
      .unwrap()
      .as_f64()
      .unwrap() as usize
  }
}

// HardCstr

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct ObjJs(ObjDirectionJs, Function);

#[wasm_bindgen]
impl ObjJs {
  #[wasm_bindgen(constructor)]
  pub fn new(od: ObjDirectionJs, f: Function) -> Self {
    ObjJs(od, f)
  }
}

impl Obj<f64, SolutionJs> for ObjJs {
  fn obj_direction(&self) -> ObjDirection {
    self.0.to_original()
  }

  fn result(&self, solution: &SolutionJs) -> f64 {
    let array: Array = Array::new();
    solution.0.iter().for_each(|&x| {
      let jv = JsValue::from_f64(x);
      array.push(&jv);
    });
    self
      .1
      .call1(&JsValue::NULL, &JsValue::from(array))
      .unwrap()
      .as_f64()
      .unwrap()
  }
}

// ObjDirectionJs

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub enum ObjDirectionJs {
  Max,
  Min,
}

impl ObjDirectionJs {
  fn to_original(&self) -> ObjDirection {
    match self {
      ObjDirectionJs::Max => ObjDirection::Max,
      ObjDirectionJs::Min => ObjDirection::Min,
    }
  }
}

// OptFacadeJs

pub type OptFacadeJsType = OptFacade<HardCstrJs, DomainJs, ObjJs, f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Debug)]
pub struct OptFacadeJs(OptFacadeJsType);

#[wasm_bindgen]
impl OptFacadeJs {
  pub fn opt_problem(self) -> OptProblemJs {
    OptProblemJs(self.0.opt_problem())
  }

  pub fn solve(self) -> Self {
    OptFacadeJs(
      self.0.solve_with(Spea2Adapter::new(
        Pct::from_percent(50),
        GeneticAlgorithmParamsBuilder::new()
          .crossover(MultiPoint::new(1, Pct::from_percent(70)))
          .mating_selection(Tournament::new(2))
          .mutation(RandomDomainAssignments::new(1, Pct::from_percent(20)))
          .build(),
      )),
    )
  }
}

// OptFacadeBuilderJs

pub type OptFacadeBuilderJsType =
  OptFacadeBuilder<HardCstrJs, DomainJs, RandomInitialSolutions, ObjJs, f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Debug)]
pub struct OptFacadeBuilderJs(OptFacadeBuilderJsType);

#[wasm_bindgen]
impl OptFacadeBuilderJs {
  #[wasm_bindgen(constructor)]
  pub fn new() -> Self {
    OptFacadeBuilderJs(OptFacadeBuilder::<
      HardCstrJs,
      DomainJs,
      RandomInitialSolutions,
      ObjJs,
      f64,
      SolutionJs,
    >::new())
  }

  pub fn build(self) -> OptFacadeJs {
    let risj = RandomInitialSolutions::new();
    OptFacadeJs(self.0.initial_solutions(risj).build())
  }

  pub fn max_iterations(self, max_iterations: usize) -> Self {
    OptFacadeBuilderJs(self.0.max_iterations(max_iterations))
  }

  pub fn objs_goals(self, objs_goals: Vec<f64>) -> Self {
    OptFacadeBuilderJs(self.0.objs_goals(objs_goals))
  }

  pub fn opt_problem(self, opt_problem: OptProblemJs) -> Self {
    OptFacadeBuilderJs(self.0.opt_problem(opt_problem.0))
  }

  pub fn stagnation_percentage(self, stagnation_percentage: PctJs) -> Self {
    OptFacadeBuilderJs(self.0.stagnation_percentage(stagnation_percentage.0))
  }

  pub fn stagnation_threshold(self, stagnation_threshold: usize) -> Self {
    OptFacadeBuilderJs(self.0.stagnation_threshold(stagnation_threshold))
  }
}

// OptProblemJs

pub type OptProblemJsType = OptProblem<HardCstrJs, DomainJs, ObjJs, f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OptProblemJs(OptProblemJsType);

#[wasm_bindgen]
impl OptProblemJs {
  pub fn with_capacity(definitions: OptProblemDefinitionsJs) -> Self {
    let results_num = definitions.0.results_num();
    let results = OptProblemResultsJsType::with_capacity(results_num, &definitions.0);
    OptProblemJs(OptProblemJsType::new(definitions.0, results))
  }

  pub fn definitions(self) -> OptProblemDefinitionsJs {
    OptProblemDefinitionsJs(self.0.into_parts().0)
  }

  pub fn results(self) -> OptProblemResultsJs {
    OptProblemResultsJs(self.0.into_parts().1)
  }
}

// OptProblemDefinitionsJs

pub type OptProblemDefinitionsJsType =
  OptProblemDefinitions<HardCstrJs, DomainJs, ObjJs, f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OptProblemDefinitionsJs(OptProblemDefinitionsJsType);

// OptProblemDefinitionsBuilderJs

pub type OptProblemDefinitionsBuilderJsType =
  OptProblemDefinitionsBuilder<HardCstrJs, DomainJs, ObjJs, f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OptProblemDefinitionsBuilderJs(OptProblemDefinitionsBuilderJsType);

#[wasm_bindgen]
impl OptProblemDefinitionsBuilderJs {
  #[wasm_bindgen(constructor)]
  pub fn new() -> Self {
    OptProblemDefinitionsBuilderJs(OptProblemDefinitionsBuilderJsType::new())
  }

  pub fn build(self) -> OptProblemDefinitionsJs {
    OptProblemDefinitionsJs(self.0.build())
  }

  pub fn domain(self, domain: DomainJs) -> Self {
    OptProblemDefinitionsBuilderJs(self.0.domain(domain))
  }

  pub fn push_hard_cstr(self, hard_cstr: HardCstrJs) -> Self {
    OptProblemDefinitionsBuilderJs(self.0.push_hard_cstr(hard_cstr))
  }

  pub fn push_obj(self, obj: ObjJs) -> Self {
    OptProblemDefinitionsBuilderJs(self.0.push_obj(obj))
  }

  pub fn results_num(self, results_num: usize) -> Self {
    OptProblemDefinitionsBuilderJs(self.0.results_num(results_num))
  }

  pub fn vars_num(self, min: usize, max: usize) -> Self {
    OptProblemDefinitionsBuilderJs(self.0.vars_num(min..max))
  }
}

// OptProblemResultsJs

pub type OptProblemResultsJsType = OptProblemResults<f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct OptProblemResultsJs(OptProblemResultsJsType);

#[wasm_bindgen]
impl OptProblemResultsJs {
  pub fn best(&self) -> OptProblemResultJs {
    OptProblemResultJs(self.0.best().unwrap().to_vec())
  }

  pub fn get(&self, idx: usize) -> OptProblemResultJs {
    OptProblemResultJs(self.0.get(idx).to_vec())
  }

  pub fn len(&self) -> usize {
    self.0.len()
  }
}

// OptProblemResultsJs

pub type OptProblemResultJsType = OptProblemResultVec<f64, SolutionJs>;

#[wasm_bindgen]
#[derive(Debug)]
pub struct OptProblemResultJs(OptProblemResultJsType);

#[wasm_bindgen]
impl OptProblemResultJs {
  pub fn hard_cstrs_results(&self) -> Vec<u32> {
    self
      .0
      .hard_cstrs_results()
      .iter()
      .map(|&x| x as u32)
      .collect()
  }

  pub fn objs(&self) -> Vec<f64> {
    self.0.objs().to_vec()
  }

  pub fn objs_avg(&self) -> f64 {
    *self.0.objs_avg()
  }

  pub fn solution(&self) -> SolutionJs {
    *self.0.solution()
  }
}

// PctJs

#[wasm_bindgen]
#[derive(Clone, Debug)]
pub struct PctJs(Pct);

#[wasm_bindgen]
impl PctJs {
  pub fn from_decimal(pct: f64) -> Self {
    PctJs(Pct::from_decimal(pct))
  }

  pub fn from_percent(pct: f64) -> Self {
    PctJs(Pct::from_percent(pct))
  }
}

// SolutionJs

#[wasm_bindgen]
#[derive(Clone, Copy, Debug)]
pub struct SolutionJs(VecArray<[f64; 16]>);

#[wasm_bindgen]
impl SolutionJs {
  pub fn array(&self) -> Vec<f64> {
    self.0.to_vec()
  }
}

impl Solution for SolutionJs {
  fn has_var(&self, idx: usize) -> bool {
    self.0.has_var(idx)
  }

  fn inter_swap(&mut self, other: &mut Self, idx: usize) {
    self.0.inter_swap(&mut other.0, idx);
  }

  fn intra_swap(&mut self, a: usize, b: usize) {
    self.0.intra_swap(a, b);
  }

  fn nnz(&self) -> usize {
    self.0.nnz()
  }

  fn var_idx(&self, nnz_idx: usize) -> usize {
    self.0.var_idx(nnz_idx)
  }

  fn var_indcs<F>(&self, cb: F)
  where
    F: FnMut(usize),
  {
    self.0.var_indcs(cb);
  }
}
