#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![deny(missing_debug_implementations)]

#[cfg(all(feature = "wasm", target_arch = "wasm32", target_os = "unknown"))]
pub mod wasm;
