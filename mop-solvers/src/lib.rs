#![allow(clippy::bool_comparison, clippy::type_complexity)]
#![cfg_attr(not(feature = "std"), no_std)]
#![deny(missing_debug_implementations)]

#[cfg(feature = "std")]
pub mod genetic_algorithm;
#[cfg(feature = "std")]
pub mod prelude;

pub use mop_common_defs::Solver;
#[cfg(feature = "std")]
mod quality_comparator;
#[cfg(feature = "std")]
mod utils;
