use mop_blocks::mp::mpho::Morho;

use crate::genetic_algorithm::spea2::properties::Properties;

#[derive(Clone, Debug)]
pub struct ArchUnionPopul<OR, S> {
  pub results: Morho<OR, S>,
  pub props: Vec<Properties<OR>>,
}
