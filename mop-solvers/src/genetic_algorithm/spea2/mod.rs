mod arch_union_popul;
mod properties;

use crate::{
  genetic_algorithm::{
    operators::{crossover::Crossover, mating_selection::MatingSelection, mutation::Mutation},
    spea2::{arch_union_popul::ArchUnionPopul, properties::Properties},
    GeneticAlgorithmParams,
  },
  quality_comparator::{ParetoComparator, QualityComparator},
  utils::{euclidean_distance, verify_pareto_dominance},
};
use core::{
  cmp::Ordering,
  fmt::Debug,
  ops::{Div, Mul, Sub},
};
use mop_blocks::{
  mp::mpho::{Morho, Mpho, MphoEvaluators},
  prelude::{BorrowAsRef, Cstr, Obj},
  Pct,
};
use mop_common_defs::Solver;
use mop_common_deps::{
  num_traits::{NumCast, One, Pow, Zero},
  rand::distributions::uniform::SampleUniform,
};

#[derive(Debug)]
pub struct Spea2<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD> {
  arch_results: Morho<MPOR, MPS>,
  arch_u_popul: ArchUnionPopul<MPOR, MPS>,
  archive_size: usize,
  density_buffer: Vec<MPOR>,
  gap: GeneticAlgorithmParams<GAPC, GAPM, GAPMS>,
  k: usize,
  mating_pool: Morho<MPOR, MPS>,
  mpho: Mpho<MPC, MPO, MPOR, MPS, MPSD>,
}

impl<'a, GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
  Spea2<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  MPS: Copy,
  MPO: BorrowAsRef<dyn Obj<MPOR, MPS> + 'a>,
  MPOR: Copy
    + Div<MPOR, Output = MPOR>
    + Mul<MPOR, Output = MPOR>
    + NumCast
    + One
    + PartialOrd
    + Pow<MPOR, Output = MPOR>
    + Send
    + Sub<MPOR, Output = MPOR>
    + Sync
    + Zero,
{
  pub fn new(
    archive_size: Pct,
    gap: GeneticAlgorithmParams<GAPC, GAPM, GAPMS>,
    mpho: Mpho<MPC, MPO, MPOR, MPS, MPSD>,
  ) -> Self {
    let population_size = mpho.definitions().results_num();
    let archive_size = (*archive_size * population_size as f64) as usize;
    let arch_u_popul_len = archive_size + population_size;
    let arch_results = Morho::with_capacity(archive_size, mpho.definitions());
    let arch_u_popul = ArchUnionPopul {
      results: Morho::with_capacity(arch_u_popul_len, mpho.definitions()),
      props: Vec::with_capacity(arch_u_popul_len),
    };
    let mating_pool = Morho::with_capacity(population_size, mpho.definitions());
    Spea2 {
      arch_results,
      arch_u_popul,
      archive_size,
      density_buffer: Vec::with_capacity(population_size),
      gap,
      k: (arch_u_popul_len as f64).sqrt() as usize,
      mating_pool,
      mpho,
    }
  }

  fn best_result(&mut self) -> usize {
    let mut best_idx = 0;
    let (defs, results) = self.mpho.parts();
    let pc = ParetoComparator::new();
    for current_idx in 1..results.len() {
      if pc.is_better(
        defs.objs(),
        &results.get(current_idx),
        &results.get(best_idx),
      ) {
        best_idx = current_idx;
      }
    }
    best_idx
  }

  fn copy_to_archive(&mut self) {
    self.arch_results.clear();
    let (arch_u_popul, arch_results) = (&self.arch_u_popul, &mut self.arch_results);
    for (result, prop) in arch_u_popul.results.iter().zip(&arch_u_popul.props) {
      if prop.density + prop.raw_fitness < MPOR::one() {
        arch_results
          .constructor()
          .copy_result(&result)
          .commit(**result.objs_avg(), **result.solution());
      }
    }
  }

  fn environment_selection(&mut self) {
    self.copy_to_archive();
    if self.arch_results.len() < self.archive_size {
      let (aup, am) = (&self.arch_u_popul.results, &mut self.arch_results);
      aup.iter().take(self.archive_size - am.len()).for_each(|x| {
        am.constructor()
          .copy_result(&x)
          .commit(**x.objs_avg(), **x.solution())
      });
    } else if self.arch_results.len() > self.archive_size {
      self.arch_results.truncate(self.archive_size);
    }
  }

  fn fill_properties_with_default_values(&mut self) {
    let iter = (0..self.arch_u_popul.results.len()).map(|_| Properties {
      density: MPOR::zero(),
      raw_fitness: MPOR::zero(),
      strength: MPOR::zero(),
    });
    self.arch_u_popul.props.clear();
    self.arch_u_popul.props.extend(iter);
  }

  fn fitness_assignment(&mut self) {
    self.fill_properties_with_default_values();
    self.set_strength();
    self.set_raw_fitness();
    self.set_density();
  }

  fn set_density(&mut self) {
    let (props, results) = (&mut self.arch_u_popul.props, &self.arch_u_popul.results);
    for (fst_ind, first_prop) in results.iter().zip(props) {
      let density_buffer = &mut self.density_buffer;
      density_buffer.clear();
      for sec_ind in results.iter() {
        density_buffer.push(euclidean_distance(fst_ind.objs(), sec_ind.objs()));
      }
      density_buffer.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
      let divider = density_buffer[self.k] + NumCast::from(2).unwrap();
      first_prop.density = MPOR::one() / divider;
    }
  }

  fn set_strength(&mut self) {
    let objs = self.mpho.definitions().objs();
    let (props, results) = (&mut self.arch_u_popul.props, &self.arch_u_popul.results);
    for (fst_idx, fst_ind) in results.iter().enumerate() {
      for (sec_idx, sec_ind) in results.iter().enumerate().skip(fst_idx) {
        let vpd = verify_pareto_dominance(objs, fst_ind.objs(), sec_ind.objs());
        match vpd {
          Ordering::Greater => {
            props[fst_idx].strength = props[fst_idx].strength + MPOR::one();
          }
          Ordering::Less => {
            let vpd = verify_pareto_dominance(objs, sec_ind.objs(), fst_ind.objs());
            if vpd == Ordering::Greater {
              props[sec_idx].strength = props[sec_idx].strength + MPOR::one();
            }
          }
          _ => {}
        }
      }
    }
  }

  fn set_raw_fitness(&mut self) {
    let objs = self.mpho.definitions().objs();
    let (props, results) = (&mut self.arch_u_popul.props, &self.arch_u_popul.results);
    for (fst_idx, fst_ind) in results.iter().enumerate() {
      for (sec_idx, sec_ind) in results.iter().enumerate().skip(fst_idx) {
        let vpd = verify_pareto_dominance(objs, fst_ind.objs(), sec_ind.objs());
        match vpd {
          Ordering::Greater => {
            let s = props[fst_idx].strength;
            props[sec_idx].raw_fitness = props[sec_idx].raw_fitness + s;
          }
          Ordering::Less => {
            let vpd = verify_pareto_dominance(objs, sec_ind.objs(), fst_ind.objs());
            if vpd == Ordering::Greater {
              let s = props[sec_idx].strength;
              props[fst_idx].raw_fitness = props[fst_idx].raw_fitness + s;
            }
          }
          _ => {}
        }
      }
    }
  }
}

impl<'a, GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD> Solver
  for Spea2<GAPC, GAPM, GAPMS, MPC, MPO, MPOR, MPS, MPSD>
where
  GAPC: Crossover<Morho<MPOR, MPS>>,
  GAPM: Mutation<MPSD, Morho<MPOR, MPS>>,
  GAPMS: MatingSelection<[MPO], Morho<MPOR, MPS>>,
  MPC: BorrowAsRef<dyn Cstr<MPS> + 'a>,
  MPO: BorrowAsRef<dyn Obj<MPOR, MPS> + 'a>,
  MPOR: Copy
    + Debug
    + Div<MPOR, Output = MPOR>
    + Mul<MPOR, Output = MPOR>
    + NumCast
    + One
    + PartialOrd
    + Pow<MPOR, Output = MPOR>
    + SampleUniform
    + Send
    + Sub<MPOR, Output = MPOR>
    + Sync
    + Zero,
  MPS: Copy + Send + Sync,
  MPSD: Send + Sync,
{
  type Rslt = Mpho<MPC, MPO, MPOR, MPS, MPSD>;

  fn after_iter(&mut self) {
    let filling_num = self.mpho.definitions().results_num();
    self.gap.mating_selection.mating_selection(
      self.mpho.definitions().objs(),
      &mut self.arch_results,
      &mut self.mating_pool,
      filling_num,
    );
    self
      .gap
      .crossover
      .crossover(&mut self.mating_pool, self.mpho.results_mut(), filling_num);

    let (defs, results) = self.mpho.parts_mut();
    self
      .gap
      .mutation
      .mutation(defs.solution_domain_mut(), results);
  }

  fn before_iter(&mut self) {
    {
      let (defs, results) = self.mpho.parts_mut();
      let ar = &mut self.arch_results;
      MphoEvaluators::eval_results(defs, ar);
      MphoEvaluators::eval_results(defs, results);
    }
    self.arch_u_popul.results.clear();
    self.arch_u_popul.results.extend(&self.arch_results);
    self.arch_u_popul.results.extend(&self.mpho.results_mut());
    self.fitness_assignment();
    self.environment_selection();
    let best_idx = self.best_result();
    self.mpho.results_mut().set_best(best_idx);
  }

  fn into_result(self) -> Self::Rslt {
    self.mpho
  }

  fn result(&self) -> &Self::Rslt {
    &self.mpho
  }

  fn result_mut(&mut self) -> &mut Self::Rslt {
    &mut self.mpho
  }
}
