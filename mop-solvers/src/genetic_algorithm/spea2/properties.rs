#[derive(Clone, Debug)]
pub struct Properties<T> {
  pub density: T,
  pub raw_fitness: T,
  pub strength: T,
}
