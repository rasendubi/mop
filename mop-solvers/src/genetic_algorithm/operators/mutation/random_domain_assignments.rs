use crate::genetic_algorithm::operators::mutation::Mutation;
use mop_blocks::{
  mp::mpho::Morho,
  prelude::{Solution, SolutionDomain},
  Pct,
};
use mop_common_deps::rand::{rngs::OsRng, Rng};

#[derive(Clone, Debug)]
pub struct RandomDomainAssignments {
  genes_num: usize,
  probability: Pct,
}

impl RandomDomainAssignments {
  pub fn new(genes_num: usize, probability: Pct) -> Self {
    RandomDomainAssignments {
      genes_num,
      probability,
    }
  }
}
impl<OR, S, SD> Mutation<SD, Morho<OR, S>> for RandomDomainAssignments
where
  S: Solution,
  SD: SolutionDomain<S>,
{
  fn mutation(&self, sd: &SD, source: &mut Morho<OR, S>) {
    let mut rng = OsRng::new().unwrap();
    for mut result in source.iter_mut() {
      if self.probability.is_in_rnd_pbty(&mut rng) {
        for _ in 0..self.genes_num {
          let nnz_idx = rng.gen_range(0, result.solution().nnz());
          let var_idx = result.solution().var_idx(nnz_idx);
          sd.set_rnd_solution_domain(result.solution_mut(), var_idx, &mut rng);
        }
      }
    }
  }
}
