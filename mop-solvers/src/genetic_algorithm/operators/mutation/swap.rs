use crate::{genetic_algorithm::operators::mutation::Mutation, utils::two_dist_rnd_num};
use mop_blocks::{mp::mpho::Morho, prelude::Solution, Pct};
use mop_common_deps::rand::rngs::OsRng;

#[derive(Clone, Debug)]
pub struct Swap {
  genes_num: usize,
  probability: Pct,
}

impl Swap {
  pub fn new(genes_num: usize, probability: Pct) -> Self {
    Swap {
      genes_num,
      probability,
    }
  }
}
impl<M, OR, S> Mutation<M, Morho<OR, S>> for Swap
where
  S: Solution,
{
  fn mutation(&self, _: &M, source: &mut Morho<OR, S>) {
    let mut rng = OsRng::new().unwrap();
    for mut individual in source.iter_mut() {
      let nnz = individual.solution_mut().nnz();
      if self.probability.is_in_rnd_pbty(&mut rng) || nnz < 2 {
        for _ in 0..self.genes_num {
          let [f_nnz_idx, s_nnz_idx] = two_dist_rnd_num(&mut rng, 0..nnz);
          let f_var_idx = individual.solution().var_idx(f_nnz_idx);
          let s_var_idx = individual.solution().var_idx(s_nnz_idx);
          individual.solution_mut().intra_swap(f_var_idx, s_var_idx);
        }
      }
    }
  }
}
