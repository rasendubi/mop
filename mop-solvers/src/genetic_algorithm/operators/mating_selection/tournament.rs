use crate::{
  genetic_algorithm::operators::mating_selection::MatingSelection,
  quality_comparator::{ParetoComparator, QualityComparator},
};
use mop_blocks::{
  mp::mpho::Morho,
  prelude::{BorrowAsRef, Obj, Zero},
};
use mop_common_deps::rand::{distributions::uniform::SampleUniform, rngs::OsRng, Rng};

#[derive(Clone, Debug, Default)]
pub struct Tournament {
  n: usize,
}

impl Tournament {
  pub fn new(n: usize) -> Self {
    assert!(n > 0);
    Tournament { n }
  }
}
impl<'a, O, OR, S> MatingSelection<[O], Morho<OR, S>> for Tournament
where
  O: BorrowAsRef<Obj<OR, S> + 'a>,
  OR: Copy + PartialOrd + SampleUniform + Zero,
  S: Copy,
{
  fn mating_selection(
    &self,
    objs: &[O],
    source: &mut Morho<OR, S>,
    destination: &mut Morho<OR, S>,
    filling_num: usize,
  ) {
    destination.clear();
    let pc = ParetoComparator::new();
    let mut rng = OsRng::new().unwrap();
    while destination.len() < filling_num {
      let mut winner = source.get(rng.gen_range(0, source.len()));
      for _ in 1..self.n {
        let current = source.get(rng.gen_range(0, source.len()));
        if pc.is_better(objs, &current, &winner) {
          winner = current;
        }
      }
      destination
        .constructor()
        .copy_result(&winner)
        .commit(**winner.objs_avg(), **winner.solution());
    }
  }
}
