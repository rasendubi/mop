#[derive(Clone, Debug)]

pub struct GeneticAlgorithmParams<C, M, MS> {
  pub crossover: C,

  pub mating_selection: MS,

  pub mutation: M,
}
