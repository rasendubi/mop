use crate::{quality_comparator::QualityComparator, utils::verify_pareto_dominance};
use core::{cmp::Ordering, marker::PhantomData};
use mop_blocks::{
  mp::mpho::orho::OrhoRef,
  prelude::{BorrowAsRef, Obj},
  ObjDirection,
};

#[derive(Debug)]
pub struct ParetoComparator<M, T>
where
  M: ?Sized,
{
  phantom: PhantomData<(T, M)>,
}

impl<M, T> ParetoComparator<M, T>
where
  M: ?Sized,
{
  pub fn new() -> Self {
    ParetoComparator {
      phantom: PhantomData,
    }
  }
}

impl<'a, 'b, O, OR, S> ParetoComparator<[O], OrhoRef<'a, OR, S>>
where
  O: BorrowAsRef<Obj<OR, S> + 'b>,
{
  fn all_objs_have_the_same_direction(objs: &[O]) -> (bool, ObjDirection) {
    let od = objs.first().unwrap().borrow().obj_direction();
    let mut has_single_direction = true;
    for obj in objs.iter().skip(1) {
      if obj.borrow().obj_direction() != od {
        has_single_direction = false;
        break;
      }
    }
    (has_single_direction, od)
  }

  fn do_is_best<F>(a: &OrhoRef<'a, OR, S>, b: &OrhoRef<'a, OR, S>, has_better_objs: F) -> bool
  where
    F: Fn(&OrhoRef<'a, OR, S>, &OrhoRef<'a, OR, S>) -> bool,
  {
    let a_violations: usize = a.hard_cstrs_results().iter().sum();
    let b_violations: usize = b.hard_cstrs_results().iter().sum();
    let a_is_feasible_and_b_is_not = b_violations > 0 && a_violations == 0;
    let both_are_infeasible_and_a_has_less_violations = || {
      let both_are_infeasible = a_violations > 0 && b_violations > 0;
      both_are_infeasible && a_violations < b_violations
    };
    let both_are_feasible_and_a_dominates_b = || {
      let both_are_feasible = a_violations == 0 && b_violations == 0;
      both_are_feasible && has_better_objs(a, b)
    };
    a_is_feasible_and_b_is_not
      || both_are_infeasible_and_a_has_less_violations()
      || both_are_feasible_and_a_dominates_b()
  }
}

impl<'a, 'b, O, OR, S> QualityComparator<[O], OrhoRef<'a, OR, S>>
  for ParetoComparator<[O], OrhoRef<'a, OR, S>>
where
  O: BorrowAsRef<Obj<OR, S> + 'b>,
  OR: PartialOrd,
{
  fn is_better(&self, objs: &[O], a: &OrhoRef<'a, OR, S>, b: &OrhoRef<'a, OR, S>) -> bool {
    let (has_same_direction, od) = Self::all_objs_have_the_same_direction(objs);
    if has_same_direction {
      Self::do_is_best(&a, &b, |a, b| {
        od.partial_cmp(a.objs_avg(), b.objs_avg()).unwrap() == Ordering::Greater
      })
    } else {
      Self::do_is_best(&a, &b, |a, b| {
        verify_pareto_dominance(objs, a.objs(), b.objs()) == Ordering::Greater
      })
    }
  }
}
