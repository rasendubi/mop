mod pareto_comparator;

pub use self::pareto_comparator::ParetoComparator;

pub trait QualityComparator<M, T>
where
  M: ?Sized,
{
  fn is_better(&self, misc: &M, a: &T, b: &T) -> bool;
}
